{
RuleID:1
ProtocolID:1438
Protocol:TCP
RuleRecMethod:SinglePacket
SrcIP:134.34.165.147
Port:[80,8080,8181]
Regex:\.swig\.
DstPort:80
Str:www.swig.org
Str:Mozilla
}
{
RuleID:2
ProtocolID:3233
Protocol:UDP
RuleRecMethod:SinglePacket
SrcIP:[134.34.165.140-134.34.165.150]
DstIP:[-134.34.3.10]
SrcPort:[62799,63208]
DstPort:[50-60]
Str:[\xb9\xe9,\xac\xb1] 0
Str:\x01\x00 2
Str:\x00\x01 4
Str:www\x06dabeaz\x03com
}
{
RuleID:3
ProtocolID:9192
Protocol:UDP
RuleRecMethod:SinglePacket
IP:[134.34.165.168,134.34.165.169]
DstIP:[224.0.0.0-]
Port:626
Str:diwan 8
}
{
RuleID:4
ProtocolID:9192
Protocol:TCP
RuleRecMethod:SinglePacket
Str:favicon.ico
Regex:(https?://)([\d\w.-]+\.)([\d\w-]{2,})+(/Doc\d.\d/)
}
