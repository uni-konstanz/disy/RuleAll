#!/usr/bin/env python
###
# Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""
Calculates ruleset statistics.
"""
import ruleset
import sys, optparse
        
def main(argv):
        # cmdline parser
        cmdparser = ruleset.Parser.add_cmdline_options()
        if len(argv)<=1:
                print cmdparser.print_help()
                sys.exit(0)
        opts,args = cmdparser.parse_args(argv[1:])
        
        # parse rule set
        print "Parsing rule set: %s" % opts.ruleset
        ruleparser = ruleset.Parser(opts.ruleset)
        rules = ruleparser.parse()
        print "Errors: %s" %ruleparser.errors
        print
        
        # calculate stats
        stat = ruleset.RuleStats.calculate(rules)
        
        print stat
        return stat, rules
        

if __name__ == '__main__':
        stat, rules = main(sys.argv)
