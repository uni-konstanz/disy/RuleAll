*FAIR LICENSE*

Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.


# RuleAll - Ruleset analyzer for packet classification.

This software is used to analyze and optimize certain rulesets for traffic classification. The rules must have a specific format that is shown in the file `test/test_rules.txt`.

The software can parse the rules, write them to other formats, calculate and print statistical information, preprocess and optimize the rules, and use the rules and `libpcap` to do software classification on packet traces or live capture data.

Three main programs are implemented.

* statistics

	Parses the rules and computes statistical information.

* preprocessor

	Parses the rules and performs certain preprocessing steps to optimize the ruleset.

* classify

	Software classifier that uses the rules and libpcap to classify captured data.

## Test

The directory `test` includes some test rules and captured data to test try the software.
All programs support standard command line options like `-h | --help` to get information on how to use it. Also, the classifier uses pcap-like command line arguments.

Examples:

	# ./classify
	Usage: classify.py [options]

	Options:
	  -h, --help  show this help message and exit
	  -r RFILE    pcap input file. default: ""
	  -i DEV      listen interface. default: pcap_lookupdev()
	  -p          dont put interface into promiscuous mode.
	  -m TO_MS    read timeout in milliseconds. default: 100
	  -s SNAPLEN  max number of bytes to capture for each packet. No limit: -1
		          default: -1
	  -w WFILE    pcap output file. default: ""
	  -c COUNT    number of packets to capture. No Limit: 0 default: 0
	  -f FILTER   pcap filter string, enclose in "". default: ""
	  -z RULESET  ruleset file name default: ""

	# ./classify -z ../test/test_rules.txt

	# ./preprocessor.py -z ../test/test_rules.txt

	# ./statistics.py -z ../test/test_rules.txt

## REQUIREMENTS

*	[libpcap-dev](http://www.tcpdump.org)
*   [dpkt](http://code.google.com/p/dpkt/)
*   [python-libpcap](http://sourceforge.net/projects/pylibpcap/)

